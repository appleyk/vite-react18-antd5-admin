export const tw = {
  common: {
    title: "SpringBoot3後台管理系統",
    chinese: "中文簡體",
    chinesetw: "中文繁體",
    english: "英語",
  },
  sideMenu: {
    home: "主頁",
    userManagement: "用戶管理",
    roleManagement: "角色管理",
    purviewManagement: "權限管理",
    dataManagement: "數據管理",
    fieldManagement: "字段管理",
    templateManagement: "模闆管理",
  },
};
