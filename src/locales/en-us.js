export const en = {
  common: {
    title: "SpringBoot3·Admin",
    chinese: "cn",
    chinesetw: "tw",
    english: "en",
  },
  sideMenu: {
    home: "Home",
    userManagement: "User",
    roleManagement: "Role",
    purviewManagement: "Purview",
    dataManagement: "Data",
    fieldManagement: "Field",
    templateManagement: "Template",
  },
};
